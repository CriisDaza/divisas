package com.example.divisas;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner spinner1=findViewById(R.id.spinner1);
        ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(this, R.array.divisas, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinner1.setAdapter(adapter);

        Spinner spinner2=findViewById(R.id.spinner2);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinner2.setAdapter(adapter);

        TextView txt_result = findViewById(R.id.txt_result);
        EditText txt_value = findViewById(R.id.txt_value);

        Button btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int vs1= spinner1.getSelectedItemPosition();
                int vs2= spinner2.getSelectedItemPosition();
                double v1 = Double.parseDouble(txt_value.getText().toString());
                switch (vs1) {

                    case 0:
                        switch (vs2) {

                            case 0:
                                   txt_result.setText(txt_value.getText());
                                break;
                            case 1:
                                  txt_result.setText(Double.toString(v1*0.00026));
                                break;
                            case 2:
                                txt_result.setText(Double.toString(v1*0.00023));
                                break;
                            case 3:
                                txt_result.setText(Double.toString(v1*0.0053));
                                break;
                            case 4:
                                txt_result.setText(Double.toString(v1*0.00019));
                                break;
                            case 5:
                                txt_result.setText(Double.toString(v1*0.30));
                                break;
                        }
                        break;
                    case 1:

                        switch (vs2) {
                            case 0:
                                txt_result.setText(Double.toString(v1*3918.43));
                                break;
                            case 1:
                                txt_result.setText(txt_value.getText());
                                break;
                            case 2:
                                txt_result.setText(Double.toString(v1*0.88));
                                break;
                            case 3:
                                txt_result.setText(Double.toString(v1*20.68));
                                break;
                            case 4:
                                txt_result.setText(Double.toString(v1*0.74));
                                break;
                            case 5:
                                txt_result.setText(Double.toString(v1*1181.94));
                                break;
                        }
                        break;
                    case 2:
                        switch (vs2) {
                            case 0:
                                txt_result.setText(Double.toString(v1*4437.68));
                                break;
                            case 1:
                                txt_result.setText(Double.toString(v1*1.13));
                                break;
                            case 2:
                                txt_result.setText(txt_value.getText());
                                break;
                            case 3:
                                txt_result.setText(Double.toString(v1*23.41));
                                break;
                            case 4:
                                txt_result.setText(Double.toString(v1*0.84));
                                break;
                            case 5:
                                txt_result.setText(Double.toString(v1*1338.26));
                                break;
                        }
                        break;
                    case 3:
                        switch (vs2) {
                            case 0:
                                txt_result.setText(Double.toString(v1*189.58));
                                break;
                            case 1:
                                txt_result.setText(Double.toString(v1*0.089));
                                break;
                            case 2:
                                txt_result.setText(Double.toString(v1*0.043));
                                break;
                            case 3:
                                txt_result.setText(txt_value.getText());
                                break;
                            case 4:
                                txt_result.setText(Double.toString(v1*0.036));
                                break;
                            case 5:
                                txt_result.setText(Double.toString(v1*57.15));
                                break;
                        }
                        break;
                    case 4:
                        switch (vs2) {
                            case 0:
                                txt_result.setText(Double.toString(v1*5286.87));
                                break;
                            case 1:
                                txt_result.setText(Double.toString(v1*1.35));
                                break;
                            case 2:
                                txt_result.setText(Double.toString(v1*1.19));
                                break;
                            case 3:
                                txt_result.setText(Double.toString(v1*27.89));
                                break;
                            case 4:
                                txt_result.setText(txt_value.getText());
                                break;
                            case 5:
                                txt_result.setText(Double.toString(v1*1594.88));
                                break;
                        }
                        break;
                    case 5:
                        switch (vs2) {
                            case 0:
                                txt_result.setText(Double.toString(v1*3.32));
                                break;
                            case 1:
                                txt_result.setText(Double.toString(v1*0.00085));
                                break;
                            case 2:
                                txt_result.setText(Double.toString(v1*0.00075));
                                break;
                            case 3:
                                txt_result.setText(Double.toString(v1*0.017));
                                break;
                            case 4:
                                txt_result.setText(Double.toString(v1*0.00067));
                                break;
                            case 5:
                                txt_result.setText(txt_value.getText());
                                break;
                        }
                        break;

                }
            }
        });



    }
}